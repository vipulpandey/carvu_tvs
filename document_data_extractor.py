'''
    This .py file contains helpful class that allows one to extract tables form pdf and doc file.
    - the code has dependency on Ghostscript and some os packages, hence better to run without virtual environment.

'''
import camelot
import re


class TableExtractor:
    ''' 
        should have statics : input file type ( pdf, doc), number of input files, files location
    '''

    def __init__(self, is_folder = False):
        self.is_folder = is_folder

class PdfTableExtractor(TableExtractor):
    
    def pdf_table_extractor(self):
        
        '''
            input : file location
            output : (num of tables extracted, extracted tables location ) 
        '''
        input_file = 'input\Workcenter 23 - Data file.pdf'
        tables = camelot.read_pdf(input_file, pages = 'all')

        # number of tables extracted
        print("Total tables extracted:", tables.n)

        table_name = ""


        # print the second table as Pandas DataFrame
        # print(tables[2].df)

        # exporting all tables at once
        output_location = "output/tvs_report.csv"

        tables.export(output_location, f='csv')
        return("output files are in {} \n & total number of tables extracted are {}".format(output_location, tables.n))  # path of extracted tables

class DocTableExtractor(TableExtractor):

    def doc_table_extractor(self):
        pass


pdf_ext = PdfTableExtractor()
print(pdf_ext.pdf_table_extractor())
