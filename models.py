'''
    Data models for tvs pdfs
    
'''

import psycopg2
conn = psycopg2.connect("host=localhost dbname=postgres user=postgres password=postgres")
cur = conn.cursor()
# "page4_table3",
table_name = "page_5_table_3"
query = """
    CREATE TABLE page_5_table_3(
    serial_id integer PRIMARY KEY,
    ipvolt_V real,
    ipcurrent_A real,
    frequency_hz real,
    powerfactor real,
    ippower_w real,
    opvolt_v real,
    opcurrent_A real,
    oppower_W real,
    efficiency_per real,
    thd_per_f real,
    temp1_degreeC real,
    temp2_degreeC real
)
"""
# create data model for table 1 ( tvs_report-page-4-table-2.csv)
cur.execute(query)


conn.commit()