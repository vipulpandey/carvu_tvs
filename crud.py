'''
    Script for reading data from csv and pushing it to table
    Here is the updated code
    // new branch
'''

import csv
import psycopg2
conn = psycopg2.connect("host=localhost dbname=postgres user=postgres password=postgres")

cur = conn.cursor()
with open('output/tvs_report-page-4-table-3.csv', 'r') as f:
    reader = csv.reader(f)
    next(reader) # Skip the header row.
    for row in reader:
        cur.execute(
        "INSERT INTO page_5_table_3 VALUES (%s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s)",
        row
    )
conn.commit()
