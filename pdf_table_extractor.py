import camelot
import re

def get_table_from_pdf():
    '''
     input : file location
     output : (num of tables extracted, extracted tables location ) 
    '''
    input_file = 'input\Workcenter 23 - Data file.pdf'
    tables = camelot.read_pdf(file, pages = 'all')

    # number of tables extracted
    print("Total tables extracted:", tables.n)

    table_name = ""


    # print the second table as Pandas DataFrame
    # print(tables[2].df)

    # exporting all tables at once
    output_location = "output/tvs_report.csv"

    tables.export(output_location, f='csv')
    return(output_location)  # path of extracted tables





